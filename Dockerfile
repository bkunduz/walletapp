FROM openjdk:8-jdk-alpine
EXPOSE 8080
ADD target/app-1.0-SNAPSHOT.jar /app/app.jar
ENTRYPOINT ["java","-jar", "/app/app.jar"]