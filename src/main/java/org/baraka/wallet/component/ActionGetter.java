package org.baraka.wallet.component;

import org.baraka.wallet.constants.enums.TransactionType;
import org.baraka.wallet.request.UserActionRequest;
import org.springframework.http.ResponseEntity;

public interface ActionGetter {
    boolean accept(TransactionType transactionType);
    ResponseEntity doOperation(UserActionRequest request);
}
