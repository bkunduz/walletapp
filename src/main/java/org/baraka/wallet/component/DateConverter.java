package org.baraka.wallet.component;

import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class DateConverter {

    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private DateFormat DATE_TIME_FORMATTER = new SimpleDateFormat(DATE_TIME_FORMAT);

    public Date toDateTime(String value) {
        try {
            return DATE_TIME_FORMATTER.parse(value);
        } catch (ParseException e) {
            return new Date(); // if date is no parseable new date is returned to reduce errors
        }
    }
}
