package org.baraka.wallet.component;

import org.baraka.wallet.constants.enums.AssetType;
import org.baraka.wallet.entity.Asset;
import org.baraka.wallet.service.AssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class InitialLoader implements
        ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    AssetService assetService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        //because db is empty each time app starts
        createAsset(AssetType.TETHER, (double) 1000);
        createAsset(AssetType.BITCOIN, (double) 0);
        createAsset(AssetType.ETHEREUM, (double) 0);

    }
    private void createAsset(AssetType assetType,Double amount) {
        Asset asset = new Asset();
        asset.setAmount(amount);
        asset.setAssetType(assetType);
        assetService.save(asset);
    }

}
