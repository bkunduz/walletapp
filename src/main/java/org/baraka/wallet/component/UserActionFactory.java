package org.baraka.wallet.component;

import org.baraka.wallet.constants.enums.TransactionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserActionFactory {

    @Autowired
    private List<ActionGetter> userActionGetter;

    public ActionGetter getActionService(TransactionType transactionType) {
        return userActionGetter.stream().filter(actionGetter -> actionGetter.accept(transactionType)).findFirst().get();
    }
}

