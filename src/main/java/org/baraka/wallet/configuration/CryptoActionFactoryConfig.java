package org.baraka.wallet.configuration;

import org.baraka.wallet.component.ActionGetter;
import org.baraka.wallet.service.UserBuyService;
import org.baraka.wallet.service.UserSellService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

@Configuration
public class CryptoActionFactoryConfig {

    @Autowired
    UserBuyService userBuyService;

    @Autowired
    UserSellService userSellService;

    @Bean
    List<ActionGetter> userActionGetter(){
        return Arrays.asList(userBuyService,userSellService);
    }
}
