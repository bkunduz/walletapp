package org.baraka.wallet.constants.enums;

import java.util.Arrays;

public enum AssetType {
    BITCOIN(0) {
        public String toString() {
            return "BITCOIN";
        }
    },
    ETHEREUM(1) {
        public String toString() {
            return "ETHEREUM";
        }
    },
    TETHER(2) {
        public String toString() {
            return "TETHER";
        }
    };

    private final Integer code;

    AssetType(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public static AssetType valueOf(Integer code){
        return Arrays.stream(AssetType.values()).filter(assetType -> assetType.getCode() == code).findFirst().get();
    }

}
