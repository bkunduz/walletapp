package org.baraka.wallet.constants.enums;

public enum CryptoCurrency {
    BITCOIN(0) {
        public String toString() {
            return "BITCOIN";
        }
    },
    ETHEREUM(1) {
        public String toString() {
            return "ETHEREUM";
        }
    };

    private final Integer code;

    private CryptoCurrency(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
