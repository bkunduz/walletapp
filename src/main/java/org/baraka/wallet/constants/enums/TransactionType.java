package org.baraka.wallet.constants.enums;

public enum TransactionType{
    SELL(0) {
        public String toString() {
            return "SELL";
        }
    },
    BUY(1) {
        public String toString() {
            return "BUY";
        }
    };

    private final Integer code;

    private TransactionType(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

}
