package org.baraka.wallet.constants.swagger;

public class SwaggerConstants {
    public static final String CRYPTO_CURRENCY = "BITCOIN OR ETHEREUM";
    public static final String CRYPTO_EXAMPLE = "BITCOIN";
    public static final String END_DATE_DESCRIPTION = "End Date of the action period, yyyy-MM-dd HH:mm:ss";
    public static final String END_DATE_EXAMPLE = "2020-04-05 23:13:12";
    public static final String START_DATE_EXAMPLE = "2020-04-05 23:13:12";
    public static final String START_DATE_DESCRIPTION = "Start Date of the action period, yyyy-MM-dd HH:mm:ss";
}
