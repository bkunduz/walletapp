package org.baraka.wallet.controller;

import org.baraka.wallet.constants.enums.AssetType;
import org.baraka.wallet.request.ActionHistoryRequest;
import org.baraka.wallet.service.AssetService;
import org.baraka.wallet.service.UserActionService;
import org.baraka.wallet.validators.annotation.ValidEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Validated
@RestController
@RequestMapping("/info")
public class InfoController {

    @Autowired
    AssetService assetService;

    @Autowired
    UserActionService userActionService;

    @GetMapping("/remaining")
    public ResponseEntity<?> getRemaining(@ValidEnum (acceptedValues = {"BITCOIN","ETHEREUM","TETHER"},message = "endpoint must be one of be one of 'BITCOIN','ETHEREUM','TETHER'")@RequestParam("code") String code) {
        return ResponseEntity.ok(assetService.findByAssetType(AssetType.valueOf(code)).getAmount());
    }

    @PostMapping("/history")
    public ResponseEntity<?> listTransactionHistory(@Valid @RequestBody ActionHistoryRequest request) {
        return ResponseEntity.ok(userActionService.findByCreatedAtAfterAndCreatedAtBefore(request));
    }

}
