package org.baraka.wallet.controller;

import org.baraka.wallet.component.UserActionFactory;
import org.baraka.wallet.constants.enums.TransactionType;
import org.baraka.wallet.request.UserActionRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class UserActionController {

    @Autowired
    UserActionFactory userActionFactory;

    @PostMapping("/transaction")
    public ResponseEntity<?> transAction(@Valid @RequestBody UserActionRequest request){
        return userActionFactory.getActionService(TransactionType.valueOf(request.getTransactionType())).doOperation(request);
    }
}
