package org.baraka.wallet.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserActionDTO {
        private Double exchangeRate;// e.g. 1 bitcoin =  515.213K Tether
        private String transactionType;
        private String cryptoCurrency;
        private Double amountCurrency;//in terms of crypto-currency, namely bitcoin or ethereum
        private Double amountTether;//in terms of Tether
        private Date createdAt;
}
