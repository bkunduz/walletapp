package org.baraka.wallet.entity;

import lombok.Getter;
import lombok.Setter;
import org.baraka.wallet.constants.enums.AssetType;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "user_asset")
public class Asset {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Enumerated
    @Column(name = "asset_type",nullable = false)
    private AssetType assetType;

    @Column(name = "amount", scale = 3,nullable = false)
    private Double amount;
}
