package org.baraka.wallet.entity;

import lombok.Getter;
import lombok.Setter;
import org.baraka.wallet.constants.enums.CryptoCurrency;
import org.baraka.wallet.constants.enums.TransactionType;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "user_actions")
public class UserAction extends Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Enumerated
    @Column(name = "transaction_type",nullable = false)
    private TransactionType transactionType;

    @Enumerated
    @Column(name = "crypto_currency",nullable = false)
    private CryptoCurrency cryptoCurrency;

    @Column(name = "amount_tether",scale = 3,nullable = false)
    private Double amountTether;//in terms of Tether

    @Column(name = "amount_currency", scale = 3,nullable = false)
    private Double amountCurrency;//in terms of crypto-currency, namely bitcoin or ethereum

    @Column(name = "current_exchange_rate", scale = 3,nullable = false)
    private Double exchangeRate;// e.g. 1 bitcoin =  515.213K Tether
}
