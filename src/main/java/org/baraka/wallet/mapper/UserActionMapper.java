package org.baraka.wallet.mapper;

import org.baraka.wallet.dto.UserActionDTO;
import org.baraka.wallet.entity.UserAction;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
@Component
public interface UserActionMapper {
    UserActionDTO toDTO(UserAction userAction);
}
