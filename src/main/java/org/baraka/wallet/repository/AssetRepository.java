package org.baraka.wallet.repository;

import org.baraka.wallet.constants.enums.AssetType;
import org.baraka.wallet.entity.Asset;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AssetRepository extends JpaRepository<Asset, Long> {

    Optional<Asset> findById(Long id);

    Asset findByAssetType(AssetType assetType);

}
