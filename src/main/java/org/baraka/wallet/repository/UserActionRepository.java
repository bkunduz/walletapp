package org.baraka.wallet.repository;

import org.baraka.wallet.entity.UserAction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface UserActionRepository extends JpaRepository<UserAction, Long> {

    Page<UserAction> findByCreatedAtAfterAndCreatedAtBefore(Date start, Date end, Pageable pageable);

}
