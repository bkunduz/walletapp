package org.baraka.wallet.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.baraka.wallet.constants.swagger.SwaggerConstants;
import org.baraka.wallet.validators.annotation.ValidDate;
import org.hibernate.validator.constraints.Range;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ActionHistoryRequest {

    @Schema(description = SwaggerConstants.START_DATE_DESCRIPTION,
            example = SwaggerConstants.START_DATE_EXAMPLE, required = true)
    @ValidDate(message = "Invalid Start Date Format, Valid Format: 'yyyy-MM-dd HH:mm:ss'")
    private String startDate;

    @Schema(description = SwaggerConstants.END_DATE_DESCRIPTION,
            example = SwaggerConstants.END_DATE_EXAMPLE, required = true)
    @ValidDate(message = "Invalid End Date Format, Valid Format: 'yyyy-MM-dd HH:mm:ss'")
    private String endDate;

    @Range(min = 1,max = 50,message = "size must be greater than 0")
    private Integer size;

    @Range(min = 0,message = "size must be greater than -1")
    private Integer page;

}
