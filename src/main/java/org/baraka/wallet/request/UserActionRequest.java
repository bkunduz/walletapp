package org.baraka.wallet.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.baraka.wallet.constants.swagger.SwaggerConstants;
import org.baraka.wallet.validators.annotation.ValidAmount;
import org.baraka.wallet.validators.annotation.ValidEnum;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserActionRequest {

    @NotNull(message = "cryptoCurrency Cannot Be Empty")
    @Schema(description = SwaggerConstants.CRYPTO_CURRENCY,
            example = SwaggerConstants.CRYPTO_EXAMPLE, required = true)
    @ValidEnum(acceptedValues = {"BITCOIN","ETHEREUM"},message = "cryptoCurrency value must be one of 'BITCOIN','ETHEREUM'")
    private String cryptoCurrency;

    @NotNull(message = "transactionType Field Cannot Be Empty")
    @ValidEnum(acceptedValues = {"SELL","BUY"},message = "transactionType value must be one of 'SELL','BUY'")
    private String transactionType;

    @NotNull(message = "Amount Field Cannot Be Empty")
    @ValidAmount(message = "Amount must be greater than zero")
    private Double amount;

}
