package org.baraka.wallet.service;

import org.baraka.wallet.constants.enums.AssetType;
import org.baraka.wallet.constants.enums.CryptoCurrency;
import org.baraka.wallet.entity.Asset;
import org.baraka.wallet.repository.AssetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AssetService {

    @Autowired
    AssetRepository assetRepository;

    public Asset save(Asset asset){
        return assetRepository.save(asset);
    }

    public Asset findByAssetType(AssetType assetType){
        return assetRepository.findByAssetType(assetType);
    }

    public AssetType getAssetType(CryptoCurrency cryptoCurrency){
        return cryptoCurrency.equals(CryptoCurrency.BITCOIN) ? AssetType.BITCOIN : AssetType.ETHEREUM;
    }
}
