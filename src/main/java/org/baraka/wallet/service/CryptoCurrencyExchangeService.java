package org.baraka.wallet.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.baraka.wallet.constants.enums.CryptoCurrency;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

@Service
public class CryptoCurrencyExchangeService {
    private final String CRYPTO_EXCHANGE_URL = "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids=";
    private final String BITCOIN_URL = CRYPTO_EXCHANGE_URL + "bitcoin";
    private final String ETHEREUM_URL = CRYPTO_EXCHANGE_URL + "ethereum";
    private final String field= "current_price";

    @SneakyThrows
    public Double getCurrencyRateInstantly(CryptoCurrency cryptoCurrency){
        String url = cryptoCurrency.equals(CryptoCurrency.BITCOIN) ? BITCOIN_URL : ETHEREUM_URL;
        InputStream inputStream =  new URL(url).openStream();
        ObjectMapper mapper = new ObjectMapper();
        ArrayList arrayList = mapper.readValue(inputStream, ArrayList.class);
        return Double.valueOf(((HashMap) arrayList.get(0)).get(field).toString());
    }
}
