package org.baraka.wallet.service;

import org.baraka.wallet.component.DateConverter;
import org.baraka.wallet.constants.enums.CryptoCurrency;
import org.baraka.wallet.constants.enums.TransactionType;
import org.baraka.wallet.dto.UserActionDTO;
import org.baraka.wallet.entity.UserAction;
import org.baraka.wallet.mapper.UserActionMapper;
import org.baraka.wallet.repository.UserActionRepository;
import org.baraka.wallet.request.ActionHistoryRequest;
import org.baraka.wallet.request.UserActionRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserActionService {

    @Autowired
    UserActionRepository userActionRepository;

    @Autowired
    DateConverter dateConverter;

    @Autowired
    UserActionMapper userActionMapper;

    public UserAction saveUserAction(UserActionRequest userActionRequest,Double currentRate, Double totalAmount){
        UserAction userAction =  new UserAction();
        userAction.setExchangeRate(currentRate);
        userAction.setAmountTether(totalAmount);
        userAction.setAmountCurrency(userActionRequest.getAmount());
        userAction.setCryptoCurrency(CryptoCurrency.valueOf(userActionRequest.getCryptoCurrency()));
        userAction.setTransactionType(TransactionType.valueOf(userActionRequest.getTransactionType()));
        userAction = userActionRepository.save(userAction);
        return userAction;
    }

    public Page<UserActionDTO> findByCreatedAtAfterAndCreatedAtBefore(ActionHistoryRequest request){
        Pageable pageable = PageRequest.of(request.getPage(), request.getSize(), Sort.by("createdAt").ascending());
        return userActionRepository.findByCreatedAtAfterAndCreatedAtBefore(dateConverter.toDateTime(request.getStartDate()),dateConverter.toDateTime(request.getEndDate()),pageable).map(userActionMapper::toDTO);
    }

    public UserAction save(UserAction userAction) {
        return userActionRepository.save(userAction);
    }

}
