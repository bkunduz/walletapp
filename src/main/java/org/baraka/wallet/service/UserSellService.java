package org.baraka.wallet.service;

import org.baraka.wallet.component.ActionGetter;
import org.baraka.wallet.constants.enums.AssetType;
import org.baraka.wallet.constants.enums.CryptoCurrency;
import org.baraka.wallet.constants.enums.TransactionType;
import org.baraka.wallet.entity.Asset;
import org.baraka.wallet.request.UserActionRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserSellService implements ActionGetter {

    @Autowired
    AssetService assetService;

    @Autowired
    CryptoCurrencyExchangeService cryptoCurrencyExchangeService;

    @Autowired
    UserActionService userActionService;

    @Override
    public boolean accept(TransactionType transactionType) {
        return TransactionType.SELL.equals(transactionType);
    }

    @Override
    public ResponseEntity<?> doOperation(UserActionRequest request) {
        Asset cryptoAsset = assetService.findByAssetType(assetService.getAssetType(CryptoCurrency.valueOf(request.getCryptoCurrency())));
        if (request.getAmount() > cryptoAsset.getAmount()) {
            return new ResponseEntity("Insufficient " + request.getCryptoCurrency() + " Balance", HttpStatus.OK);
        }
        Double currentRate = cryptoCurrencyExchangeService.getCurrencyRateInstantly(CryptoCurrency.valueOf(request.getCryptoCurrency()));
        cryptoAsset.setAmount(cryptoAsset.getAmount() - request.getAmount());
        assetService.save(cryptoAsset);

        Asset tetherAsset = assetService.findByAssetType(AssetType.TETHER);
        Double totalAmountInTermsOfTether = currentRate * request.getAmount();
        tetherAsset.setAmount(tetherAsset.getAmount() + totalAmountInTermsOfTether);
        assetService.save(tetherAsset);
        userActionService.saveUserAction(request, currentRate, totalAmountInTermsOfTether);

        return ResponseEntity.ok("Success");
    }
}
