package org.baraka.wallet.validators.annotation;

import org.baraka.wallet.validators.validator.AmountValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = AmountValidator.class)
public @interface ValidAmount {

    String message() default "Invalid Amount";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
