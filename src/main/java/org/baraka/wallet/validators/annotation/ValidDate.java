package org.baraka.wallet.validators.annotation;

import org.baraka.wallet.validators.validator.DateValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.METHOD, ElementType.FIELD , ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = DateValidator.class)
public @interface ValidDate {
    String format() default "yyyy-MM-dd HH:mm:ss";

    String message() default "Invalid Date Format, Valid Format: 'yyyy-MM-dd HH:mm:ss'";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}