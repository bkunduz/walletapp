package org.baraka.wallet.validators.annotation;

import org.baraka.wallet.validators.validator.EnumValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.METHOD, ElementType.FIELD,ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = EnumValidator.class)
public @interface ValidEnum {
    String[] acceptedValues();

    String message() default "Invalid Enums";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
