package org.baraka.wallet.validators.validator;

import org.baraka.wallet.validators.annotation.ValidAmount;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AmountValidator implements ConstraintValidator<ValidAmount, Double> {

    @Override
    public void initialize(ValidAmount constraintAnnotation) {
    }

    @Override
    public boolean isValid(Double amount, ConstraintValidatorContext constraintValidatorContext) {
        try
        {
        return amount > new Double(0);
        }
        catch (Exception e){
            return false;
        }
    }
}
