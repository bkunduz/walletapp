package org.baraka.wallet.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.baraka.wallet.request.ActionHistoryRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class InfoControllerTests {

    public static final String REMAINING_PATH = "/info/remaining";
    public static final String HISTORY_PATH = "/info/history";
    private static final ObjectMapper om = new ObjectMapper();


    @Autowired
    private MockMvc mockMvc;


    @SneakyThrows
    @Test
    public void test400ForHistory() {//test invalid request
        ActionHistoryRequest actionHistoryRequest = new ActionHistoryRequest();
        actionHistoryRequest.setEndDate("2021-04-05 10:00:00");
        actionHistoryRequest.setStartDate("2021-04-05 10:00:00");
        actionHistoryRequest.setPage(-1);
        actionHistoryRequest.setSize(-1);

        mockMvc.perform(post(HISTORY_PATH)
                .content(om.writeValueAsString(actionHistoryRequest))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("Validation Failed")));
    }

    @Test
    public void test200ForHistory() throws Exception {//test valid request
        ActionHistoryRequest actionHistoryRequest = new ActionHistoryRequest();
        actionHistoryRequest.setEndDate("2021-04-05 10:00:00");
        actionHistoryRequest.setStartDate("2021-04-05 11:00:00");
        actionHistoryRequest.setPage(1);
        actionHistoryRequest.setSize(10);

        mockMvc.perform(post(HISTORY_PATH)
                .content(om.writeValueAsString(actionHistoryRequest))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @SneakyThrows
    @Test
    public void test400ForRemaining() {//test invalid request
        ActionHistoryRequest actionHistoryRequest = new ActionHistoryRequest();
        actionHistoryRequest.setEndDate("2021-04-05 10:00:00");
        actionHistoryRequest.setStartDate("2021-04-05 10:00:00");
        actionHistoryRequest.setPage(-1);
        actionHistoryRequest.setSize(-1);

        mockMvc.perform(get(REMAINING_PATH)
                .param("code","DOLAR")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("Validation Failed")));
    }

    @Test
    public void test200ForRemaining() throws Exception {//test valid request
        ActionHistoryRequest actionHistoryRequest = new ActionHistoryRequest();
        actionHistoryRequest.setEndDate("2021-04-05 10:00:00");
        actionHistoryRequest.setStartDate("2021-04-05 11:00:00");
        actionHistoryRequest.setPage(1);
        actionHistoryRequest.setSize(10);

        mockMvc.perform(get(REMAINING_PATH)
                .param("code","TETHER")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }
}
