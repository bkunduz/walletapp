package org.baraka.wallet.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.baraka.wallet.request.ActionHistoryRequest;
import org.baraka.wallet.request.UserActionRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserActionControllerTests {

    public static final String TRANSACTION_PATH = "/api/transaction";
    private static final ObjectMapper om = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;
    @SneakyThrows
    @Test
    public void test400() {//test invalid request
        UserActionRequest userActionRequest = new UserActionRequest();
        userActionRequest.setAmount(0.1);
        userActionRequest.setCryptoCurrency("ASDAS");
        userActionRequest.setTransactionType("BUYYYY");

        mockMvc.perform(post(TRANSACTION_PATH)
                .content(om.writeValueAsString(userActionRequest))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("Validation Failed")));
    }

    @Test
    public void test200() throws Exception {//test valid request
        UserActionRequest userActionRequest = new UserActionRequest();
        userActionRequest.setAmount(0.0001);
        userActionRequest.setCryptoCurrency("BITCOIN");
        userActionRequest.setTransactionType("BUY");

        mockMvc.perform(post(TRANSACTION_PATH)
                .content(om.writeValueAsString(userActionRequest))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }


}
