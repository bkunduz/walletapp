package org.baraka.wallet.service;

import org.baraka.wallet.constants.enums.AssetType;
import org.baraka.wallet.constants.enums.CryptoCurrency;
import org.baraka.wallet.entity.Asset;
import org.baraka.wallet.request.UserActionRequest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserBuyServiceTest {

    Asset tetherAsset;
    UserActionRequest userActionRequest;

    @Mock
    AssetService assetService;

    @Mock
    CryptoCurrencyExchangeService cryptoCurrencyExchangeService;

    @InjectMocks
    UserBuyService userBuyService;


    @Before
    public void init(){
        tetherAsset = new Asset();
        tetherAsset.setAmount(0.0001);
        tetherAsset.setAssetType(AssetType.TETHER);

        userActionRequest = new UserActionRequest();
        userActionRequest.setAmount(10.56);
        userActionRequest.setCryptoCurrency("BITCOIN");
        userActionRequest.setTransactionType("BUY");
    }

    @Test
    public void enoughBalanceTest(){
        Mockito.doReturn(150000.876).when(cryptoCurrencyExchangeService).getCurrencyRateInstantly(CryptoCurrency.BITCOIN);
        Mockito.doReturn(tetherAsset).when(assetService).findByAssetType(AssetType.TETHER);
        Assert.assertEquals("Insufficient TETHER Balance",userBuyService.doOperation(userActionRequest).getBody());
    }
}
