package org.baraka.wallet.service;

import org.baraka.wallet.constants.enums.AssetType;
import org.baraka.wallet.constants.enums.CryptoCurrency;
import org.baraka.wallet.entity.Asset;
import org.baraka.wallet.request.UserActionRequest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserSellServiceTest {

    Asset bitcoinAsset;
    UserActionRequest userActionRequest;

    @Mock
    AssetService assetService;

    @Mock
    CryptoCurrencyExchangeService cryptoCurrencyExchangeService;

    @InjectMocks
    UserSellService userSellService;

    @Before
    public void init(){
        bitcoinAsset = new Asset();
        bitcoinAsset.setAmount(10.0);
        bitcoinAsset.setAssetType(AssetType.BITCOIN);

        userActionRequest = new UserActionRequest();
        userActionRequest.setAmount(150.01);
        userActionRequest.setCryptoCurrency("BITCOIN");
        userActionRequest.setTransactionType("SELL");
    }

    @Test
    public void enoughBalanceTest(){
        Mockito.doReturn(bitcoinAsset).when(assetService).findByAssetType(Mockito.any());
        Assert.assertEquals("Insufficient BITCOIN Balance",userSellService.doOperation(userActionRequest).getBody());
    }

}
